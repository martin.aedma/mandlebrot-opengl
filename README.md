# mandlebrot-opengl

Mandlebrot set drawn with OpenGL using C++

Dependencies included (GLAD, GLFW, GLM)

Use Arrow keys to move around :: LEFT CTRL to ZOOM IN :: LEFT SHIFT to ZOOM OUT

![](mandlebrot.jpg)


